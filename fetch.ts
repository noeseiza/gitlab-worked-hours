import { config } from "https://deno.land/x/dotenv/mod.ts";
import { parse } from "https://deno.land/std@0.86.0/flags/mod.ts";

const args = parse(Deno.args);
await config({ safe: true });
const apiKey = config().GITLAB_TOKEN;
const gitlabUrl = config().GITLAB_URL;
const uname = config().GITLAB_UNAME;
const api = "api/v4/issues";
let total: number = 0;
const paramLength = Object.keys(args).length;
const params = {
  assignee_username: uname,
  per_page: "100",
  "labels[]": "",
};

const apiCall = async (params: string) => {
  const myHeaders = new Headers();
  myHeaders.append("PRIVATE-TOKEN", apiKey);
  myHeaders.append("Content-Type", "application/json");
  const resp = await fetch(`https://${gitlabUrl}/${api}?${params}`, {
    method: "GET",
    headers: myHeaders,
  });
  return resp.json();
};

const mLabels = args.s ? args.labels.split(",") : args.labels;

console.log(`Task \t URL \t Estimate \t Spend`);
if (args.s && paramLength > 1) {
  await Promise.all(mLabels.map(async (lab: string) => {
    const labels = `${lab}${args.q ? ",Quality Assurance" : ""}`;
    params["labels[]"] = labels;
    const urlParams = new URLSearchParams(params).toString();
    //console.dir(`Querying api with labels: ${labels}`);
    
    const resp = await apiCall(urlParams);
    resp.map((elem: any) => {
      total += elem.time_stats.time_estimate;
      console.log(`${elem.title}\t${elem.web_url}\t${elem.time_stats.time_estimate/3600}\t${elem.time_stats.total_time_spent/3600}`);
    });
  }));
} else if (paramLength > 1) {
  const labels = `${mLabels}${args.q ? ",Quality Assurance" : ""}`;
  params["labels[]"] = labels;
  const urlParams = new URLSearchParams(params).toString();
  //console.dir(`Querying api with labels: ${labels}`);
  const resp = await apiCall(urlParams);
  resp.map((elem: any) => {
    total += elem.time_stats.time_estimate;
    console.log(`${elem.title}\t${elem.web_url}\t${elem.time_stats.time_estimate/3600}\t${elem.time_stats.total_time_spent/3600}`);
  });
} else {
  console.dir("Please specify at least of those params: --labels , -q, -s");
  console.dir("Example: ./fetch --labels=W#10,W#11 -q -s");
}
//console.log(`Worked hours : ${total / 3600}h`);